name := """example-app"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava, PlayEbean, SbtWeb)

scalaVersion := "2.11.7"

libraryDependencies ++= Seq(
  javaJdbc,
  cache,
  javaWs,
  evolutions,
  "org.webjars" %% "webjars-play" % "2.5.0",
  "org.webjars" % "react-router" % "1.0.3",
  "org.webjars" % "react" % "15.0.1",
  "org.webjars" % "jquery" % "2.2.3"
)

//http://www.webjars.org Все webjars

//Для обработки requirejs
pipelineStages := Seq(rjs)