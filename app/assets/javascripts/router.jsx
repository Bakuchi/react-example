define(['react', 'react-dom', 'react-router', './components/TaskBox'],
    function (React, ReactDOM, ReactRouter, TaskBox) {
        var Route = ReactRouter.Route;
        var Router = ReactRouter.Router;
        ReactDOM.render((
            <Router>
                <Route path="/" component={TaskBox}/>
            </Router>
        ), document.getElementById('container'));
    });