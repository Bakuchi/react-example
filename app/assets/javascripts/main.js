(function () {
    "use strict";

    requirejs.config({
        shim: {
            'react-router': {
                deps: ['react-dom'],
                exports: 'react-router'
            },
            'react': {
                deps: [''],
                exports: 'react'
            },
            'react-dom': {
                deps: ['react'],
                exports: 'react-dom'
            },
            'jquery': {
                deps: [''],
                exports: ['$', 'jquery', 'jQuery']
            },
            'app': {
                deps: ['react-router']
            }
        },
        paths: {
            'jquery': ['../lib/jquery/jquery'],
            'react': ['../lib/react/react'],
            'react-router': ['../lib/react-router/ReactRouter'],
            'react-dom': ['../lib/react/react-dom'],
            'app': ['./router']
        }
    });

    require(['react', 'react-dom', 'jquery', 'app','react-router'], function () {

    });

})();