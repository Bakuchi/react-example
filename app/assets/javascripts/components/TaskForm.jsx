define(['react'], function (React) {
    var TaskForm = React.createClass({
        getInitialState: function() {
            return {title: '', descr: ''};
        },
        handleTitleChange: function(e) {
            this.setState({title: e.target.value});
        },
        handleDesrcChange: function(e) {
            this.setState({descr: e.target.value});
        },
        handleSubmit: function(e) {
            e.preventDefault();
            var title = this.state.title.trim();
            var descr = this.state.descr.trim();
            if (!descr || !title) {
                return;
            }
            this.props.onTaskSubmit({title: title, descr: descr});
            this.setState({title: '', descr: ''});
        },
        render: function() {
            return (
                <form className="taskForm" onSubmit={this.handleSubmit}>
                    <input
                        type="text"
                        placeholder="Title"
                        value={this.state.title}
                        onChange={this.handleTitleChange}
                    />
                    <input
                        type="text"
                        placeholder="Description"
                        value={this.state.descr}
                        onChange={this.handleDesrcChange}
                    />
                    <input type="submit" value="Post" />
                </form>
            );
        }
    });
    return TaskForm;
});