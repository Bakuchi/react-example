define(['react','./TaskForm','./TaskList'], function (React,TaskForm,TaskList) {
    var TaskBox = React.createClass({
        loadTasksFromServer: function() {
            $.ajax({
                url: "/tasks",
                dataType: 'json',
                cache: false,
                context: this,
                success: function(data) {
                    this.setState({data: data.tasks});
                }.bind(this),
                error: function(xhr, status, err) {
                    console.error(this.props.url, status, err.toString());
                }.bind(this)
            });
        },
        handleTaskSubmit: function(task) {
            $.ajax({
                url: "/tasks",
                dataType: 'json',
                type: 'POST',
                contentType: 'application/json; charset=utf-8',
                data: JSON.stringify(task),
                success: function(data) {
                    this.setState({data: data});
                }.bind(this),
                error: function(xhr, status, err) {
                    console.error(this.props.url, status, err.toString());
                }.bind(this)
            });
        },
        getInitialState: function() {
            return {data: []};
        },
        componentDidMount: function() {
            this.loadTasksFromServer();
            setInterval(this.loadTasksFromServer, 2000);
        },
        render: function () {
            return (
                <div className="taskBox">
                    <h1>Tasks</h1>
                    <TaskList data={this.state.data}/>
                    <TaskForm onTaskSubmit={this.handleTaskSubmit}/>
                </div>
            );
        }
    });
    return TaskBox;
});