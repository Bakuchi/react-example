define(['react', './Task'], function (React, Task) {
    var TaskList = React.createClass({
        render: function() {
            var taskNodes = this.props.data.map(function(task) {
                return (
                    <Task title={task.title}>
                        {task.description}
                    </Task>
                );
            });
            return (
                <div>
                    {taskNodes}
                </div>
            );
        }
    });
    return TaskList;
});