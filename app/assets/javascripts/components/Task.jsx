define(['react'], function (React) {
    var Task = React.createClass({
        render: function() {
            return (
                <div>
                    <p>Title:
                        <br/>
                        <strong>{this.props.title}</strong>
                    </p>
                    <p>Description:
                        <br/>
                        {this.props.children}
                    </p>
                    <hr/>
                </div>
            );
        }
    });
    return Task;
});