package models;

import com.avaje.ebean.Model;
import play.data.validation.Constraints;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Task extends Model {

    public Task(String title, String description) {
        this.title = title;
        this.description = description;
    }

    @Id
    private Long id;

    @Constraints.Required
    public String title;

    public String description;

//    public static List<Task> all() {
//        return new ArrayList<>();
//    }
    public static Find<Long,Task> find = new Find<Long,Task>(){};

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
