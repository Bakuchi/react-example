package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.inject.Inject;
import models.Task;
import play.api.mvc.*;
import play.data.Form;
import play.data.FormFactory;
import play.libs.Json;
import play.mvc.*;

import play.mvc.Controller;
import play.mvc.Result;
import views.html.*;

import java.util.List;

/**
 * This controller contains an action to handle HTTP requests
 * to the application's home page.
 */
public class HomeController extends Controller {

    public Result react(){
        return ok(index.render());
    }


    public Result getTasks() {
        ObjectNode result = Json.newObject();
        List<Task> tasks = Task.find.all();
        result.set("tasks", Json.toJson(tasks));

        return ok(result);
    }


    public Result getOneTask(Long id) {
        ObjectNode result = Json.newObject();
        Task task= Task.find.byId(id);
        result.set("task", Json.toJson(task));

        return ok(result);
    }

    public Result reactSave() {
        JsonNode json = request().body().asJson();
        if(json == null) {
            return badRequest("Expecting Json data");
        } else {
            String title = json.findPath("title").textValue();
            String descr = json.findPath("descr").textValue();
            if(title == null || descr == null) {
                return badRequest("Missing parameter");
            } else {
                Task task = new Task(title,descr);
                task.save();
                return getTasks();
            }
        }
    }



}
